#include "pch.h"
#include "TestScene.h"
#include "Cube_Object.h"
#include "EditorCamera.h"
#include "DirectionLight.h"
#include "Transform.h"

TestScene::TestScene(void) noexcept
{
}

TestScene::~TestScene(void) noexcept
{
}

bool TestScene::Init(void) noexcept
{
	//for (int i = 0; i < 10; i++)
	//{
	//	for (int j = 0; j < 10; j++)
	//	{
	GameObject* obj = GameObject::Instantiate(new Cube_Object());
	obj->GetTransform()->SetLocalScale(1, 1, 1);
	//	}
	//}
	GameObject::Instantiate(new EditorCamera(g_hWnd));
	//GameObject::Instantiate(new DirectionLight());

	return true;
}

void TestScene::FixedUpdate(float fElapsedTime) noexcept
{

}

void TestScene::Update(float fElapsedTime) noexcept
{

}

void TestScene::LateUpdate(float fElapsedTime) noexcept
{

}

void TestScene::Render(float fElapsedTime) noexcept
{

}

void TestScene::Release(void) noexcept
{

}
