#include "pch.h"
#include "Terrain.h"

namespace ce
{
	Terrain::Terrain(uint16 width, uint16 height) noexcept :
		_width(width),
		_height(height)
	{

	}

	Terrain::~Terrain(void) noexcept
	{
	}

	bool Terrain::Init(void) noexcept
	{
		return true;
	}

	void Terrain::FixedUpdate(float fElapsedTime) noexcept
	{

	}

	void Terrain::Update(float fElapsedTime) noexcept
	{

	}

	void Terrain::LateUpdate(float fElapsedTime) noexcept
	{

	}

	void Terrain::Render(void) noexcept
	{

	}

	void Terrain::Release(void) noexcept
	{

	}

}